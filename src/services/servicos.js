import axios from "axios";

const COURSE_API_URL = 'http://localhost:4200'

const servicos = axios.create({baseURL: 'http://localhost:4200'});

export default servicos;


// class ServicosDataService {

//     retrieveAllServicos() {
//         //console.log('executed service')
//         console.log(this.parametrosEntradaServico(110))

//         return axios.get(`${COURSE_API_URL}/servicos`);
//     }

//     async parametrosEntradaServico(id) {
//         //console.log('executed service')
//         const {parametrosentrada} = await axios.get(`${COURSE_API_URL}/servicos/${id}`);
//         return parametrosentrada;
//     }

//     retrieveServico(id) {
//         //console.log('executed service')
//         return axios.get(`${COURSE_API_URL}/servicos/${id}`);
//     }

//     updateServico(id, servico) {
//         //console.log('executed service')
//         return axios.put(`${COURSE_API_URL}/servicos/${id}`, servico);
//     }
//     deleteServico(id) {
//         //console.log('executed service')
//         return axios.delete(`${COURSE_API_URL}/servicos/${id}`);
//     }
//     criarServico(servico) {
//         servico.id = this.retrieveAllServicos.length;
//         return axios.post(`${COURSE_API_URL}/servicos/`, servico);
//     }
   
// }

// export default new ServicosDataService()