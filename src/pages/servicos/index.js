import React, { Component } from 'react';
import Servicos from '../../services/servicos'
import MaterialTable from 'material-table';
import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';

export class ListServicos extends Component {

    state = {
        servicos: [],
        columns: [
            { title: 'Nome do Método', field: 'metodoname' },
            { title: 'Descrição', field: 'description' },
          ],
          message: null
    }

    componentDidMount() {
        this.loadServicos();
    }

    loadServicos = async () => {
        const response = await Servicos.get('/servicos');
        this.setState({ servicos: response.data })
    }

    deleteCourseClicked = async (id) => {
      const response = await Servicos.delete(`/servicos/${id}`);
      this.setState({ servicos: response.data })
    }

    updateCourseClicked = (id) =>{
      this.props.history.push(`/servico/${id}`)
    }

    viewCourseClicked = (id) =>{
      this.props.history.push(`/servico/${id}`)
    }

    render() {
        const { servicos } = this.state;
        return (
            <div >
                <MaterialTable
                    title="Serviços Backend" 
                    columns={this.state.columns}
                    data={servicos}
                    actions ={[
                        {
                          icon: "add",
                          tooltip: 'Adicionar Curso',
                          isFreeAction: true,
                          onClick: (event) => this.addCourseClicked(),
                        },
                        {
                          icon: 'visibility',
                          tooltip: 'Editar Curso',
                          onClick: (event, rowData) => this.viewCourseClicked (rowData.id),
                        },
                        {
                            icon: 'edit',
                            tooltip: 'Editar Curso',
                            onClick: (event, rowData) => this.updateCourseClicked (rowData.id),
                          },
                        {
                          icon: 'delete',
                          tooltip: 'Delete Curso' ,
                          onClick: (event, rowData) => { if (window.confirm('Você tem certeza que quer deletar este Item')) this.deleteCourseClicked(rowData.id)}
                        }
                      ]}
                      options={{
                        actionsColumnIndex: -1,
                        pageSize: 10,
                        search: true,
                        headerStyle: {
                            backgroundColor: '#ddd',
                            color: '#333'
                          }
                      }}
                />
            </div>
        )
    }
}

export default ListServicos
