import React from 'react'
import MaterialTable from 'material-table';
import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';

export default class BasicTreeData extends React.Component {
    state = {
        columns: [
            { title: 'Nome', field: 'name' },
            { title: 'Tipo', field: 'type' },
            { title: 'Descrição', field: 'description' },
            { title: 'Obrigatoriedade', field: 'Mandatory' }
        ],
        message: null
    }

    render() {
        console.log(this.props.item)
        const parametrosEntrada = this.props.item
        return (
            <MaterialTable
                title="Parametros de Entrada"
                data={parametrosEntrada}
                columns={this.state.columns}
                editable={{
                    onRowAdd: newData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = parametrosEntrada;
                                    data.push(newData);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = parametrosEntrada;
                                    const index = data.indexOf(oldData);
                                    data[index] = newData;
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowDelete: oldData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    let data = this.parametrosSaida;
                                    const index = data.indexOf(oldData);
                                    data.splice(index, 1);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                }}
                options={{
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    rowStyle: {
                        backgroundColor: '#f2f2f2',
                    },
                    headerStyle: {
                        backgroundColor: '#dcdcdc',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    }
                }}
                parentChildData={(row, rows) => rows.find(a => a.name === row.parentName)}

            />
        )
    }
}