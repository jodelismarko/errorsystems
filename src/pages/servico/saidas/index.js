import React from 'react'
import MaterialTable from 'material-table';
import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';

export default class BasicTreeData extends React.Component {
    state = {
        columns: [
            { title: 'Nome', field: 'name' },
            { title: 'Tipo', field: 'type' },
            { title: 'Descrição', field: 'description' }
        ],
        message: null
    }

    render() {
        console.log(this.props.item)
        const parametrosSaida = this.props.item
        return (
            <MaterialTable
                title="Parametros de Saída"
                data={parametrosSaida}
                columns={this.state.columns}
                editable={{
                    onRowAdd: newData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = parametrosSaida;
                                    data.push(newData);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    const data = parametrosSaida;
                                    const index = data.indexOf(oldData);
                                    data[index] = newData;
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                    onRowDelete: oldData =>
                        new Promise((resolve, reject) => {
                            setTimeout(() => {
                                {
                                    let data = this.parametrosSaida;
                                    const index = data.indexOf(oldData);
                                    data.splice(index, 1);
                                    this.setState({ data }, () => resolve());
                                }
                                resolve()
                            }, 1000)
                        }),
                }}
                options={{
                    rowStyle: rowData => ({
                        backgroundColor: (!rowData.parentName) ? '#f2f2f2' : '#FFF'
                      }),
                    actionsColumnIndex: -1,
                    paging: false,
                    search: false,
                    headerStyle: {
                        backgroundColor: '#dcdcdc',
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    }
                }}
                parentChildData={(row, rows) => rows.find(a => a.name === row.parentName)}

            />
        )
    }
}