import React, { Component } from "react";
import Servicos from "../../services/servicos";
import ParametroSaida from "./saidas"
import "./styles.css"
import Erros from "./erros"
import ParametrosEntrada from "./entradas"

export default class Servico extends Component {
    
    state = {
        servico: [],
        parametrosentrada: [],
        parametrossaida: [],
        erros: [],
    }
    async componentDidMount() {
        const { id } = this.props.match.params;
        const response = await Servicos.get(`/servicos/${id}`);
        const { parametrosentrada, parametrossaida, erros, ...servico } = response.data;
        this.setState({ servico, parametrosentrada, parametrossaida, erros })
    }

    render() {
        const { servico, parametrosentrada, parametrossaida, erros } = this.state;
        return (
            <div className="servico-info">
                <h1>{servico.metodoname}</h1>
                <p>{servico.description}</p>
                <article><ParametrosEntrada item={parametrosentrada}/> </article>
                <article><ParametroSaida item={parametrossaida}/> </article>
                <article><Erros item={erros}/> </article>
            </div>
        )
    }
}

