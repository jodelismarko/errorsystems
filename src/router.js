import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Main from "./pages/main";
import Product from "./pages/product";
import Servicos from "./pages/servicos";
import Servico from "./pages/servico";
import ParametroSaida from './pages/servico/saidas';
import TreeTable from './pages/treeTable';





const Routers = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/products/:id" component={Product} />
            <Route path="/servicos" component={Servicos} />
            <Route path="/servico/:id" component={Servico} />
            <Route path="/parametrossaida/:id" component={ParametroSaida} />
            <Route path="/tree-table" component={TreeTable} />

        </Switch>
    </BrowserRouter>

);

export default Routers;